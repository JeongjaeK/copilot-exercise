steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE shops (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(256) NOT NULL,
            street VARCHAR(256) NOT NULL,
            city VARCHAR(100) NOT NULL,
            state VARCHAR(2) NOT NULL,
            zip VARCHAR(11) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE shops;
        """
    ],
]
