steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE employees (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR(100) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            email VARCHAR(100) NOT NULL,
            hashed_password VARCHAR(100) NOT NULL,
            shop_id INTEGER REFERENCES shops (id) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE employees;
        """
    ],
]
